import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import { EXAMPLE_DATA } from './table-datasource';
@Injectable({
  providedIn: 'root'
})
export class DataSourceService {

  constructor() { }

getCharacters(): Observable<any[]>{
  return Observable.of(EXAMPLE_DATA).delay(100);
}
getColumns(): string[]{
return ["Grua","Reporte","Fecha","Centro Costo","Operador","Area Trabajo","Programado Imprevisto","Descripcion","Horometro Inicial","Horometro Final","Horometro Total","Firma Supervisor","Traslado","Instalacion & Armado","Levante","Carga Contrapesos","Otros","Espera Cliente","Firma Documentos","Espera Izaje","Firma Izaje","Espera Contrapeso","Sin Cuadrilla","Falta Escolta","Colacion","Cambio Turno","Otros1","Recepcion Combustible","Condiciones Climaticas","Otros2","Sin Operador","Sin Tareas","Mantencion Programada","Certificacion","Revision Tecnica","Fiscalizacion","Falla Equipo","Daño Equipo","Falla Operacional","Otros3"
  ]};

}
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { DataSourceService } from '../data-source.service';
// import { TableDataSource, TableItem } from './table-datasource';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  characters: Observable<any[]>;
  columns: string[];

  constructor(private atService: DataSourceService) {  }

  ngOnInit() {
    // this.dataSource = new TableDataSource();
    this.columns = this.atService.getColumns();
    this.characters = this.atService.getCharacters();
  }


}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { HeaderComponent } from './header/header.component';
import { ManualComponent } from './manual/manual.component';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';


@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    HeaderComponent,
    ManualComponent
  ],
  imports: [
    BrowserModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    RouterModule.forRoot([
      { path: '', component: ManualComponent, data: { title: 'Horas de Uso' } },
      { path: 'manual', component: ManualComponent, data: { title: 'Manual' } },
      { path: 'gruas', component: TableComponent, data: { title: 'Gruas' } }
      //{ path: 'grua49', component: TableComponent, data: { title: 'GP-49' } },
      //{ path: 'grua54', component: TableComponent, data: { title: 'GP-54' } }
    ]),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
